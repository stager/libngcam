#include <stdio.h>

#include <ngcam.h>

#define RETURN_FO_NAME(name) \
 do \
  { \
   if (fieldOrder == NGCam::FO_##name) \
    { \
     return #name; \
    } \
  } while (0)

static const char *GetFieldOrderName (unsigned int fieldOrder)
 {
  RETURN_FO_NAME(ANY);
  RETURN_FO_NAME(NONE);
  RETURN_FO_NAME(TOP);
  RETURN_FO_NAME(BOTTOM);
  RETURN_FO_NAME(INTERLACED);
  RETURN_FO_NAME(SEQ_TB);
  RETURN_FO_NAME(SEQ_BT);
  RETURN_FO_NAME(ALTERNATE);
  RETURN_FO_NAME(INTERLACED_TB);
  RETURN_FO_NAME(INTERLACED_BT);

  return "???";
 }

typedef struct
 {
  unsigned int  cap;
  const char   *name;
 } capnames_t;

#define CAP_ENTRY(display_name) \
 { NGCam::CAP_##display_name, #display_name }

static capnames_t CapNames[] =
 {
  CAP_ENTRY(CAPTURE),
  CAP_ENTRY(VIDEO_OUTPUT),
  CAP_ENTRY(VIDEO_OVERLAY),
  CAP_ENTRY(VBI_CAPTURE),
  CAP_ENTRY(VBI_OUTPUT),
  CAP_ENTRY(SLICED_VBI_CAPTURE),
  CAP_ENTRY(SLICED_VBI_OUTPUT),
  CAP_ENTRY(RDS_CAPTURE),
  CAP_ENTRY(VIDEO_OUTPUT_OVERLAY),
  CAP_ENTRY(HW_FREQ_SEEK),
  CAP_ENTRY(TUNER),
  CAP_ENTRY(AUDIO),
  CAP_ENTRY(RADIO),
  CAP_ENTRY(MODULATOR),
  CAP_ENTRY(READWRITE),
  CAP_ENTRY(ASYNCIO),
  CAP_ENTRY(STREAMING)
 };

static void PrintCapsList (const NGCam::Device *device)
 {
  printf("Capabilities:");

  for (unsigned int i = 0; i < sizeof(CapNames) / sizeof(CapNames[0]); i++)
   {
    if (device->IsCapSupported(CapNames[i].cap))
     {
      printf(" %s",CapNames[i].name);
     }
   }

  printf("\n");
 }

int main (int /*argc*/, char * /*argv*/ [])
 {
  unsigned int deviceCount = NGCam::System::GetDeviceCount();

  printf("\n");
  printf("Total device number: %u\n",deviceCount);

  for (unsigned int deviceIndex = 0; deviceIndex < deviceCount; deviceIndex++)
   {
    NGCam::Device *device = NGCam::System::OpenDevice(deviceIndex);

    printf("\n");
    printf("Name: %s\n",device->GetName());

    unsigned int width;
    unsigned int height;
    unsigned int pixelFormat;
    unsigned int fieldOrder;
    unsigned int bytesPerLine;

    device->GetCurrentFormat(&width,&height,&pixelFormat,&fieldOrder,&bytesPerLine);

    printf("Current image format:\n");
    printf("Size: %ux%u\n",width,height);
    printf("Pixel format: %c%c%c%c\n",
           pixelFormat & 0xFF,
           (pixelFormat >> 8) & 0xFF,
           (pixelFormat >> 16) & 0xFF,
           pixelFormat >> 24);
    printf("Field order: %s\n",GetFieldOrderName(fieldOrder));
    printf("Bytes per line: %u\n",bytesPerLine);
    PrintCapsList(device);

    delete device;
   }

  return 0;
 }

