#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <FreeImage.h>

#include <ngcam.h>

static void ShowUsage ()
 {
  printf("\nUsage: capture <device_index> <dest_file>\n\n");
 }

static bool SaveImage (const char          *fileName,
                       const unsigned char *buffer,
                       unsigned int         width,
                       unsigned int         height)
 {
  FIBITMAP *bitmap = FreeImage_Allocate(width,height,24);

  memcpy(FreeImage_GetBits(bitmap),buffer,width * height * 3);

  bool ok = FreeImage_Save(FIF_PNG,bitmap,fileName,0);

  FreeImage_Unload(bitmap);

  FIBITMAP *bitmap2 = FreeImage_Allocate(1,1,24);

  unsigned char *b = FreeImage_GetBits(bitmap2);

  b[0] = 0xFF;
  b[1] = 0x00;
  b[2] = 0x00;

  FreeImage_Save(FIF_PNG,bitmap,"rgb.png",0);

  FreeImage_Unload(bitmap);


  return ok;
 }

static bool Capture (const char *destFileName, unsigned int deviceIndex)
 {
  bool ok = false;

  try
   {
    NGCam::Device *device = 0;

    try
     {
      device = NGCam::System::OpenDevice(deviceIndex);

      unsigned int width,height,pixelFormat;

      device->GetCurrentFormat(&width,&height,&pixelFormat,0,0);
      device->SetCurrentFormat(width,height,pixelFormat,NGCam::FO_NONE);
      device->GetCurrentFormat(&width,&height,0,0,0);

      unsigned char *buffer = new unsigned char[width * height * 3];

      device->CaptureRGB(buffer);

      ok = SaveImage(destFileName,buffer,width,height);

      delete buffer;

      delete device; device = 0;
     }
    catch (...)
     {
      if (device != 0)
       {
        delete device;
       }

      throw;
     }
   }
  catch (NGCam::Exception &e)
   {
    fprintf(stderr,"error: %s\n",e.GetMessage());
   }

  return ok;
 }

int main (int argc, char *argv[])
 {
  if (argc == 2 && !strcmp(argv[1],"-h"))
   {
    ShowUsage();

    return 0;
   }

  if (argc != 3)
   {
    fprintf(stderr,"error: invalid arguments, use -h to display help information\n");

    return 1;
   }

  errno = 0;

  long deviceIndex = strtol(argv[1],NULL,10);

  if (errno != 0)
   {
    fprintf(stderr,"error: invalid device index - integer expected\n");

    return 1;
   }

  if (deviceIndex < 0)
   {
    fprintf(stderr,"error: invalid device index - must be non-negative integer\n");

    return 1;
   }

  bool ok = Capture(argv[2],(unsigned int)deviceIndex);

  return ok ? 0 : 1;
 }

