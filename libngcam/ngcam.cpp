#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "ngcam.h"

namespace NGCam {

static unsigned char ColorFloatToUChar         (float c)
 {
  if (c < 0.0f)
   {
    return 0;
   }
  else if (c > 255.0f)
   {
    return 255;
   }
  else
   {
    return (unsigned char)(c + 0.5f);
   }
 }

static void YUV2RGB                   (unsigned char      *r,
                                       unsigned char      *g,
                                       unsigned char      *b,
                                       unsigned char       y,
                                       unsigned char       u,
                                       unsigned char       v)
 {
  float y1 = (255.0f / 219.0f) * ((int)y - 16);
  float pr = (255.0f / 224.0f) * ((int)u - 128);
  float pb = (255.0f / 224.0f) * ((int)v - 128);

  float rf = y1               + 1.402f * pr;
  float gf = y1 - 0.344f * pb - 0.714f * pr;
  float bf = y1 + 1.772f * pb;

  *r = ColorFloatToUChar(rf);
  *g = ColorFloatToUChar(gf);
  *b = ColorFloatToUChar(bf);
 }

const char*
RuntimeException::OUT_OF_MEMORY_MESSAGE = "out of memory";

static bool CopyFramePackedYUVToRGB   (unsigned char      *rgbBuffer,
                                       const unsigned char*srcBuffer,
                                       unsigned int        srcBytesPerLine,
                                       unsigned int        width,
                                       unsigned int        height,
                                       int                 y0Offset,
                                       int                 y1Offset,
                                       int                 crOffset,
                                       int                 cbOffset)
 {
  if (height == 0)
   {
    return true;
   }

  const unsigned char *src = srcBuffer + srcBytesPerLine * (height - 1);
  unsigned char       *dst = rgbBuffer;

  for (unsigned int y = 0; y < height; y++)
   {
    for (unsigned int x = 0; x < width / 2; x++)
     {
      unsigned char y0 = src[x * 4 + y0Offset];
      unsigned char y1 = src[x * 4 + y1Offset];
      unsigned char cr = src[x * 4 + crOffset];
      unsigned char cb = src[x * 4 + cbOffset];

      YUV2RGB(&dst[2],&dst[1],dst,y0,cr,cb); dst += 3;
      YUV2RGB(&dst[2],&dst[1],dst,y1,cr,cb); dst += 3;
     }

    src -= srcBytesPerLine;
   }

  return true;
 }

static bool CopyFrameToRGB            (unsigned char      *rgbBuffer,
                                       const unsigned char*srcBuffer,
                                       unsigned int        srcPixelFormat,
                                       unsigned int        srcBytesPerLine,
                                       unsigned int        width,
                                       unsigned int        height)
 {
  if      (srcPixelFormat == PF_VYUY)
   {
    return CopyFramePackedYUVToRGB(rgbBuffer,srcBuffer,srcBytesPerLine,width,height,1,3,0,2);
   }
  else if (srcPixelFormat == PF_YUYV)
   {
    return CopyFramePackedYUVToRGB(rgbBuffer,srcBuffer,srcBytesPerLine,width,height,0,2,3,1);
   }
  else
   {
    return false;
   }
 }

RuntimeException::RuntimeException    (const char         *message)
 {
  this->message = strdup(message);

  if (this->message == NULL)
   {
    this->message = OUT_OF_MEMORY_MESSAGE;
   }
 }

RuntimeException::~RuntimeException   ()
 {
  if (message != OUT_OF_MEMORY_MESSAGE)
   {
    free((void*)message);
   }
 }

const char*
RuntimeException::GetMessage          ()
 {
  return message;
 }

Device::Device                        (const char         *fileName)
 {
  handle = open(fileName,O_RDWR);

  if (handle == -1)
   {
    throw RuntimeException("cannot open device");
   }

  memset(&caps,0,sizeof(caps));

  if (ioctl(handle,VIDIOC_QUERYCAP,&caps) != 0)
   {
    close(handle);

    throw RuntimeException("cannot open device - capabilities query failed");
   }
 }

Device::~Device                       ()
 {
  close(handle);
 }

const char*
Device::GetName                       () const
 {
  return (const char*)caps.card;
 }

void
Device::GetCurrentFormat              (unsigned int       *width,
                                       unsigned int       *height,
                                       unsigned int       *pixelFormat,
                                       unsigned int       *fieldOrder,
                                       unsigned int       *bytesPerLine) const
 {
  struct v4l2_format format;

  memset(&format,0,sizeof(format));

  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl(handle,VIDIOC_G_FMT,&format) != 0)
   {
    throw RuntimeException("cannot get current picture format");
   }

  if (width != 0)
   {
    *width = format.fmt.pix.width;
   }

  if (height != 0)
   {
    *height = format.fmt.pix.height;
   }

  if (pixelFormat != 0)
   {
    *pixelFormat = format.fmt.pix.pixelformat;
   }

  if (fieldOrder != 0)
   {
    *fieldOrder = format.fmt.pix.field;
   }

  if (bytesPerLine != 0)
   {
    *bytesPerLine = format.fmt.pix.bytesperline;
   }
 }

bool
Device::IsCapSupported                (unsigned int        cap) const
 {
  return (cap & caps.capabilities) != 0;
 }

void
Device::SetCurrentFormat              (unsigned int        width,
                                       unsigned int        height,
                                       unsigned int        pixelFormat,
                                       unsigned int        fieldOrder)
 {
  struct v4l2_format format;

  memset(&format,0,sizeof(format));

  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  format.fmt.pix.width        = width;
  format.fmt.pix.height       = height;
  format.fmt.pix.pixelformat  = pixelFormat;
  format.fmt.pix.field        = fieldOrder;

  format.fmt.pix.bytesperline = 0;

  if (ioctl(handle,VIDIOC_S_FMT,&format) != 0)
   {
    throw RuntimeException("cannot set current picture format");
   }
 }

bool
Device::CaptureFrameRead              (void               *dst,
                                       size_t              size)
 {
  ssize_t resultSize = read(handle,dst,size);

  return resultSize == (ssize_t)size;
 }

bool
Device::CaptureFrameUserPtr           (void               *dst,
                                       size_t              size)
 {
  struct v4l2_buffer buf;

  memset(&buf,0,sizeof(buf));

  buf.type      = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory    = V4L2_MEMORY_USERPTR;
  buf.m.userptr = (unsigned long)dst;
  buf.length    = size;

  if (ioctl(handle,VIDIOC_QBUF,&buf) != 0)
   {
    return false;
   }

  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  bool ok = ioctl(handle,VIDIOC_STREAMON,&type) == 0;

  memset(&buf,0,sizeof(buf));

  buf.type      = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory    = V4L2_MEMORY_USERPTR;

  ok = ok && ioctl(handle,VIDIOC_DQBUF,&buf) == 0;

  ioctl(handle,VIDIOC_STREAMOFF,&type);

  return ok;
 }

bool
Device::CaptureFrameMMap              (void               * /*dst*/,
                                       size_t              /*size*/)
 {
  printf("User mmap\n");

  return false;
 }

bool
Device::CaptureFrameStream            (void               *dst,
                                       size_t              size)
 {
  bool   ok = false;
  struct v4l2_requestbuffers reqbuf;

  memset(&reqbuf,0,sizeof(reqbuf));

  reqbuf.count  = 1;
  reqbuf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  reqbuf.memory = V4L2_MEMORY_USERPTR;

  if (ioctl(handle,VIDIOC_REQBUFS,&reqbuf) == 0)
   {
    ok = CaptureFrameUserPtr(dst,size);
   }
  else
   {
    if (errno == EINVAL)
     {
      ok = CaptureFrameMMap(dst,size);
     }
   }

  return ok;
 }

bool
Device::CaptureFrame                  (void               *dst,
                                       size_t              size)
 {
  if      (IsCapSupported(CAP_READWRITE))
   {
    return CaptureFrameRead(dst,size);
   }
  else if (IsCapSupported(CAP_STREAMING))
   {
    return CaptureFrameStream(dst,size);
   }
  else
   {
    return false;
   }
 }

void
Device::CaptureRGB                    (unsigned char      *buffer)
 {
  unsigned int width,height,pixelFormat,bytesPerLine;

  GetCurrentFormat(&width,&height,&pixelFormat,0,&bytesPerLine);

  size_t tempBufferSize = height * bytesPerLine;

  unsigned char *tempBuffer = new unsigned char[tempBufferSize];

  if (!CaptureFrame(tempBuffer,tempBufferSize))
   {
    delete tempBuffer;

    throw RuntimeException("frame capture failed - read error");
   }

  bool ok = CopyFrameToRGB(buffer,tempBuffer,pixelFormat,bytesPerLine,width,height);

  delete tempBuffer;

  if (!ok)
   {
    throw RuntimeException("frame capture failed - format conversion failed");
   }
 }

unsigned int
System::GetDeviceCount                ()
 {
  unsigned int n;

  for (n = 0; n < MAX_DEVICE_COUNT && DeviceExists(n); n++) ;

  return n;
 }

Device*
System::OpenDevice                    (unsigned int        index)
 {
  dev_filename_t fileName;

  GetDeviceFileName(fileName,index);

  return new Device(fileName);
 }

void
System::GetDeviceFileName             (char               *fileName,
                                       unsigned int        index)
 {
  sprintf(fileName,"/dev/video%d",index);
 }

bool
System::DeviceExists                  (unsigned int        index)
 {
  dev_filename_t fileName;

  GetDeviceFileName(fileName,index);

  return FileExists(fileName);
 }

bool
System::FileExists                    (const char         *fileName)
 {
  struct stat buf;

  return stat(fileName,&buf) == 0;
 }

};

