#ifndef __NGCAM_H__
#define __NGCAM_H__

#include <linux/videodev2.h>

namespace NGCam {

enum /* pixel format constants */
 {
  PF_RGB332  = v4l2_fourcc('R','G','B','1'),
  PF_RGB444  = v4l2_fourcc('R','4','4','4'),
  PF_RGB555  = v4l2_fourcc('R','G','B','O'),
  PF_RGB565  = v4l2_fourcc('R','G','B','P'),
  PF_RGB555X = v4l2_fourcc('R','G','B','Q'),
  PF_RGB565X = v4l2_fourcc('R','G','B','R'),
  PF_BGR24   = v4l2_fourcc('B','G','R','3'),
  PF_RGB24   = v4l2_fourcc('R','G','B','3'),
  PF_BGR32   = v4l2_fourcc('B','G','R','4'),
  PF_RGB32   = v4l2_fourcc('R','G','B','4'),

  PF_YUV444  = v4l2_fourcc('Y','4','4','4'),
  PF_YUV555  = v4l2_fourcc('Y','U','V','O'),
  PF_YUV565  = v4l2_fourcc('Y','U','V','P'),
  PF_YUV32   = v4l2_fourcc('Y','U','V','4'),

  PF_YUYV    = V4L2_PIX_FMT_YUYV,
  PF_UYVY    = v4l2_fourcc('U','Y','V','Y'),
  PF_VYUY    = V4L2_PIX_FMT_VYUY
 };

enum /* field order constants */
 {
  FO_ANY           = V4L2_FIELD_ANY,
  FO_NONE          = V4L2_FIELD_NONE,
  FO_TOP           = V4L2_FIELD_TOP,
  FO_BOTTOM        = V4L2_FIELD_BOTTOM,
  FO_INTERLACED    = V4L2_FIELD_INTERLACED,
  FO_SEQ_TB        = V4L2_FIELD_SEQ_TB,
  FO_SEQ_BT        = V4L2_FIELD_SEQ_BT,
  FO_ALTERNATE     = V4L2_FIELD_ALTERNATE,
  FO_INTERLACED_TB = V4L2_FIELD_INTERLACED_TB,
  FO_INTERLACED_BT = V4L2_FIELD_INTERLACED_BT
 };

enum
 {
  CAP_CAPTURE              = V4L2_CAP_VIDEO_CAPTURE,
  CAP_VIDEO_OUTPUT         = V4L2_CAP_VIDEO_OUTPUT,
  CAP_VIDEO_OVERLAY        = V4L2_CAP_VIDEO_OVERLAY,
  CAP_VBI_CAPTURE          = V4L2_CAP_VBI_CAPTURE,
  CAP_VBI_OUTPUT           = V4L2_CAP_VBI_OUTPUT,
  CAP_SLICED_VBI_CAPTURE   = V4L2_CAP_SLICED_VBI_CAPTURE,
  CAP_SLICED_VBI_OUTPUT    = V4L2_CAP_SLICED_VBI_OUTPUT,
  CAP_RDS_CAPTURE          = V4L2_CAP_RDS_CAPTURE,
  CAP_VIDEO_OUTPUT_OVERLAY = V4L2_CAP_VIDEO_OUTPUT_OVERLAY,
  CAP_HW_FREQ_SEEK         = V4L2_CAP_HW_FREQ_SEEK,
  CAP_TUNER                = V4L2_CAP_TUNER,
  CAP_AUDIO                = V4L2_CAP_AUDIO,
  CAP_RADIO                = V4L2_CAP_RADIO,
  CAP_MODULATOR            = V4L2_CAP_MODULATOR,
  CAP_READWRITE            = V4L2_CAP_READWRITE,
  CAP_ASYNCIO              = V4L2_CAP_ASYNCIO,
  CAP_STREAMING            = V4L2_CAP_STREAMING
 };

class Exception
 {
  public           :

  virtual         ~Exception          () {}

  virtual
  const char      *GetMessage         () = 0;
 };

class RuntimeException : public Exception
 {
  public           :

                   RuntimeException   (const char         *message);
  virtual         ~RuntimeException   ();

  virtual
  const char      *GetMessage         ();

  private          :

  const char      *message;

  static
  const char      *OUT_OF_MEMORY_MESSAGE;
 };

class Device
 {
  public           :

                   Device             (const char         *fileName);
                  ~Device             ();

  const char      *GetName            () const;

  void             GetCurrentFormat   (unsigned int       *width,
                                       unsigned int       *height,
                                       unsigned int       *pixelFormat,
                                       unsigned int       *fieldOrder,
                                       unsigned int       *bytesPerLine) const;

  bool             IsCapSupported     (unsigned int        cap) const;

  void             SetCurrentFormat   (unsigned int        width,
                                       unsigned int        height,
                                       unsigned int        pixelFormat,
                                       unsigned int        fieldOrder);

  void             CaptureRGB         (unsigned char      *buffer);
  bool             CaptureFrameRead   (void               *dst,
                                       size_t              size);
  bool             CaptureFrameUserPtr(void               *dst,
                                       size_t              size);
  bool             CaptureFrameMMap   (void               *dst,
                                       size_t              size);
  bool             CaptureFrameStream (void               *dst,
                                       size_t              size);
  bool             CaptureFrame       (void               *dst,
                                       size_t              size);

  private          :

  int                                  handle;
  struct v4l2_capability               caps;
 };

class System
 {
  public           :

  static
  unsigned int     GetDeviceCount     ();
  static Device   *OpenDevice         (unsigned int        index);

  private          :

  enum { DEVICE_FILENAME_MAX_LEN = 12 }; // "/dev/video63"
  enum { MAX_DEVICE_COUNT        = 64 };

  typedef char dev_filename_t[DEVICE_FILENAME_MAX_LEN + 1];

  static void      GetDeviceFileName  (char               *fileName,
                                       unsigned int        index);
  static bool      DeviceExists       (unsigned int        index);
  static bool      FileExists         (const char         *fileName);
 };

};

#endif

